import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.AbstractClassifier;

import java.io.IOException;

public class Wrapper {

    public static void main(String[] args) throws Exception {
        // main: creating a new wrapper for and giving the command line arguments
        CommandLineParameters command = new CommandLineParameters(args);
        Wrapper runWeka = new Wrapper();
        String fileName = command.processCommandLine();
        System.out.println(fileName);
        runWeka.run(fileName);
    }

    private void run(String fileName) throws Exception {
        Instances data;
        try {
            // tries to reading file
            DataSource source = new DataSource(fileName);
            data = source.getDataSet();
            if (data.classIndex() == -1) {
                data.setClassIndex(data.numAttributes() - 1);
            }
            // if it not passed the try then it will run this exception error
        } catch (Exception e) {
            throw new IOException("Error reading file.");
        }
        AbstractClassifier oneR = loadModel();
        ClassifyInstance(oneR, data);
    }

    private AbstractClassifier loadModel() throws Exception {
        String wekaModel = "data/OneR.model";
        return (AbstractClassifier) weka.core.SerializationHelper.read(wekaModel);
    }

    private void ClassifyInstance(AbstractClassifier concept, Instances data) throws Exception {
        Instances instances = new Instances(data);
        for (int i = 0; i < data.numInstances(); i++){
            double label = concept.classifyInstance(data.instance(i));
            instances.instance(i).setClassValue(label);
        }
        System.out.println(instances);
    }
}
