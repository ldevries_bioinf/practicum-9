import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

public class CommandLineParameters {
    String[] clArguments;
    public Options options;

    public CommandLineParameters(String[] args) {
        // save the arguments in a variable
        this.clArguments = args;
        buildOptions();
    }

    private void buildOptions() {
        // create Options object
        this.options = new Options();

        this.options.addOption("f", "file", true, "The model file");
        this.options.addOption("h", "help", true, "the help manual.");
    }

    public String processCommandLine() {
        // create a defaultParser and HelpFormatter
        CommandLineParser parser = new DefaultParser();
        HelpFormatter helpFormatter = new HelpFormatter();

        try {
            // tries to get the given arguments of the command line
            CommandLine command = parser.parse(this.options, this.clArguments);

            // when the -h in the command line is given
            if (command.hasOption("help")){
                helpFormatter.printHelp("The file testData.arff is recommended for this project",
                        this.options, true);
                return null;
                // else returns the -f given file (arff)
            }else if (command.hasOption("file")) {
                return command.getOptionValue("file");
            }
            // if it is not passing trough then it gets the exception error
        } catch (ParseException e) {
            helpFormatter.printHelp("Try to use the testData.arff file or try -h.",
                    options, true);
        }
        return null;
    }
}