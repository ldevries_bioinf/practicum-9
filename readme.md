Authors: L. de Vries
Date: November 15th 2020
Version: Bio-informatica 3, praktijk opdracht 09

# Java classifier wrapper   

# Introduction  
For this project did I make a Java Wrapper that predicts the histological response of the EoE on the data. 
With the Weka imports it is possible to use the classiefier any you wish. 
But just for this project I uses the OneR classifier. 

# Prerequisites
The program requires the following packages to be installed: 
- Rstudio_1.2.5019
- R_3.5.2
- gtools_3.8.1   
- reshape2_1.4.3 
- ggplot2_3.3.0  
- dplyr_0.8.5    
- knitr_1.28     
- haven_2.2.0 
- IntelliJ IDEA 2020.2
- Java 14.0.2
- Javac 14.0.2
- Apache cli: commons-cli:commons-cli:1.4
- Weka: weka-stable 3.8.0

# Installing
Clone the source files. Type this in the terminal ```git clone https://ldevries_bioinf@bitbucket.org/ldevries_bioinf/practicum-9.git``` 
or look at the files on the page ```https://bitbucket.org/ldevries_bioinf/practicum-9/src/master/```.

# Manual
The folder practicum-9 contains the files you use yo get the results. 
You can run the compiled file that can be found in the ```build/libs/praktijk-thema-9-wrapper-0.0.1.jar```. 
When you have opened IntelliJ IDEA you can run this in the terminal ```java -jar build/libs/praktijk-thema-9-wrapper-0.0.1.jar -f data/testData.arff```.

# Support
For information you can mail to: [Linda's email](ld.de.vries@st.hanze.nl)

# Credits
- https://bioinf.nl/~fennaf/thema08/
- Michiel Noback
- Classmates